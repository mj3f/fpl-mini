# TL;DR

The purpose of this project was to produce a series of 38 game week tables for mini leagues. This would allow you to track your progress in your mini
leagues as the season progressed.

Requires the following dependencies, install via pip:
    + fpl
    + aiohttp
    + jsonpickle

To run the script, enter 'python3 main.py' in a terminal.